import {inject, Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {OAuth2UrlDto} from "../dto/ouath2-url-dto";
import {AuthOpaqueToken} from "../dto/auth-opaque-token";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  http = inject(HttpClient);

  private apiUrl = environment.apiUrl + "/auth";

  getOAuth2Url() {
    return this.http.get<OAuth2UrlDto>(`${this.apiUrl}/url`);
  }

  getAuthOpaqueToken(code: string) {
    let params = new HttpParams();
    params = params.append("code", code);
    return this.http.get<AuthOpaqueToken>(`${this.apiUrl}/callback`, {params: params});
  }

}
