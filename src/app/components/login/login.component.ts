import {Component, inject, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{

  oAuth2Url: string = "";

  authService: AuthService = inject(AuthService);

  ngOnInit(): void {
    this.authService.getOAuth2Url().subscribe(
      value => {
        this.oAuth2Url = value.url;
      }
    );
  }

}
