import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ActivatedRoute, RouterOutlet} from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {AuthService} from "./services/auth.service";
import {HttpErrorResponse} from "@angular/common/http";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, LoginComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {

  authService = inject(AuthService);

  activatedRoute = inject(ActivatedRoute);

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (!params["code"]) {
        return;
      }
      this.authService.getAuthOpaqueToken(params["code"]!).subscribe( {
        next: (value) => {
          localStorage.setItem("opaqueToken", value.opaqueToken);
        },
        error: (err: HttpErrorResponse) => {
          console.log(err);
        }
      });
    });
  }



}
